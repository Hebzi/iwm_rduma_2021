package Lab01;

public class Main {
    public static void main(String[] args){

        Channel channel = new Channel(3, "Name", 8, 1.5, 213.7, 5.56, "unit", 42.0, 7);
        Channel channel1 = new Channel();
        System.out.println(channel);
        System.out.println("Przeskalowana wartość i-tej próbki (metoda value): " + channel.value(3));
        System.out.println("Srednia wartość tablicy (metoda mean): " + channel.mean());
        System.out.println("Srednia kwadratowa (metoda  rms): " + channel.rms());
    }
}

