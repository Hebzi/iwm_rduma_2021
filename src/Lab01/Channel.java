package Lab01;

import java.util.Arrays;

public class Channel {

    // obiekty
    private int channelNr;
    private String channelName;
    private int bits;
    private double rangeMin;
    private double rangeMax;
    private double sensitivity;
    private String unit;
    private double samplingFreq;
    private int length;
    private Short[] samples;

    //konstruktor pełny
    public Channel(int channelNr, String channelName, int bits, double rangeMin, double rangeMax, double sensitivity, String unit, double samplingFreq, int length) {
        this.channelNr = channelNr;
        this.channelName = channelName;
        this.bits = bits;
        this.rangeMin = rangeMin;
        this.rangeMax = rangeMax;
        this.sensitivity = sensitivity;
        this.unit = unit;
        this.samplingFreq = samplingFreq;
        this.length = length;
        this.samples = new Short[length];

        for (short i = 0; i < length; i++) {
            this.samples[i] = i;
        }
    }

    // konstruktor pusty
    public Channel() {
    }

    // metody
    public double value(int i) {
        return samples[i] * samplingFreq;
    }

    public double mean() {
        int sum = 0;
        for (int value : samples) {
            sum += value;
        }
        return (double) sum / samples.length;
    }

    public double rms() {
        int sum = 0;
        for (int value : samples) {
            sum += Math.pow(value, 2);
        }
        return Math.sqrt((double) sum / samples.length);
    }

    @Override
    public String toString() {
        return "Channel{" +
                "channelNr=" + channelNr +
                ", channelName='" + channelName + '\'' +
                ", bits=" + bits +
                ", rangeMin=" + rangeMin +
                ", rangeMax=" + rangeMax +
                ", sensitivity=" + sensitivity +
                ", unit='" + unit + '\'' +
                ", samplingFreq=" + samplingFreq +
                ", length=" + length +
                ", samples=" + Arrays.toString(samples) +
                '}';
    }
}
