package Lab02;

import java.util.Arrays;

public abstract class Sequence<T> extends Packet {


    // pola
    protected int channelNr;
    protected String unit;
    protected double resolution;
    protected T[] buffer;

    public Sequence(String device, String description, long date, int channelNr, String unit, double resolution, T[] buffer) {
        super(device, description, date);
        this.channelNr = channelNr;
        this.unit = unit;
        this.resolution = resolution;
        this.buffer = buffer;
    }


    public Sequence() {
        super();
        this.channelNr = 0;
        this.unit = "DefaultUnit";
        this.resolution = 0;
        this.buffer = null;
    }


    @Override
    public String toString() {
        return "\n---|Sequence|--- " +
                "\nchannelNr: " + channelNr +
                "\nunit: " + unit +
                "\nresolution: " + resolution +
                "\nbuffer: " + Arrays.toString(buffer) +
                super.toString();
    }
}

