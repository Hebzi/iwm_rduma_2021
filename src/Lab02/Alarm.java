package Lab02;

public class Alarm extends Packet {

    private int channelNr;
    private double threshold;
    private int direction;


    public Alarm(String device, String description, long date, int channelNr, double threshold, int direction) {
        super(device, description, date);
        this.channelNr = channelNr;
        this.threshold = threshold;
        this.direction = direction;
    }

    public Alarm() {
        super("DefaultAlarm", "DefaultDescription", 43242123);
    }

    @Override
    public String toString() {
        return "Alarm:" +
                "\nchannelNr: " + channelNr +
                "\nthreshold: " + threshold +
                "\ndirection: " + direction +
                super.toString();
    }
}

