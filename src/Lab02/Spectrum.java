package Lab02;

import java.io.Serializable;
import java.util.Arrays;

public class Spectrum<T> extends Sequence<T> implements Serializable {

    // 0 - liniowa, 1 - logarytmiczna
    private final int scaling;

    public Spectrum(String device, String description, long date, int channelNr, String unit, double resolution, T[] buffer, int scaling) {
        super(device, description, date, channelNr, unit, resolution, buffer);
        this.scaling = scaling;
    }

    public Spectrum() {
        super();
        this.scaling = 0;
    }


    @Override
    public String toString() {
        return "---|Spectrum|---" +
                "\nscaling: " + scaling +
                super.toString();
    }
}

