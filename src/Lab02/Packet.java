package Lab02;

import java.io.Serializable;

public abstract class Packet implements Serializable {

    // atrybuty
    protected String device;
    protected String description;
    protected long date;

    public Packet(String device, String description, long date) {
        this.device = device;
        this.description = description;
        this.date = date;
    }

    public Packet() {
        this.device = "DefaultDev";
        this.description = "DefaultDesc";
        this.date = 0;
    }

    public String getDevice() {
        return device;
    }


    public String getDescription() {
        return description;
    }


    public long getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "\n---|Packet|---" +
                "\ndevice: " + device +
                "\ndescription: " + description +
                "\ndate: " + date;
    }
}
