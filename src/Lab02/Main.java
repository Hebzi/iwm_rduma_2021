package Lab02;

public class Main {
    public static void main(String[] args) {

        TimeHistory timeHistory = new TimeHistory("Nazwa", "Opis", 12312312, 3, "unit", 123.23, new Integer[]{1,2,3}, 123.32);
        Spectrum spectrum = new Spectrum("Nazwa", "Opis", 12312312, 3, "unit", 123.23, new Integer[]{1, 2, 3, 4}, 1);
        Alarm alarm = new Alarm("Nazwa", "Opis", 12312312, 3, 1.21, -1);

        System.out.println(timeHistory);
        System.out.println(spectrum);
        System.out.println(alarm);
/*
        TimeHistory timeHistory1 = new TimeHistory();
        Spectrum spectrum1 = new Spectrum();
        Alarm alarm1 = new Alarm();


        System.out.println(timeHistory1);
        System.out.println(spectrum1);
        System.out.println(alarm1);
*/
    }
}
