package Lab04_RMI.Klient;


import Lab04_RMI.Interfejsy.ICommunication;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

public class Client {
    private Scanner userInput = new Scanner(System.in);
    ICommunication remoteObject;
    Registry reg; // the remote objects registry
    String inputText;
    private String hostname;

    public static void main(String[] args) {
        Client client = new Client("localhost");
        client.startClient();
    }

    public Client(String hostname) {
        this.hostname = hostname;
    }

    public void startClient() {
        while (true) {
            try {
                reg = LocateRegistry.getRegistry(this.hostname);
                remoteObject = (ICommunication) reg.lookup("Server");

                switch (Tools.mainMenu()) {
                    case 1:
                        Tools.registerFrameMenu(remoteObject);
                        break;
                    case 2:
                        Tools.getData(remoteObject);
                        break;
                    case 3:
                        System.out.println("Exit from program...");
                        return;
                    default:
                        System.out.println("Incorrect option");
                        break;
                }
            } catch (RemoteException | NotBoundException e) {
                e.printStackTrace();
            }
        }
    }
}