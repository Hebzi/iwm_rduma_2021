package Lab04_RMI.Klient;

import Lab02.*;
import Lab04_RMI.Interfejsy.ICommunication;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

public class Tools {
    public static int mainMenu() {
        Scanner userInput = new Scanner(System.in);
        String input = "";
        System.out.println("--------------------MAIN MENU---------------------");
        System.out.println("[1] Register frame");
        System.out.println("[2] Get data by criteria");
        System.out.println("[3] Exit");
        System.out.println("--------------------------------------------------");
        return enterNumber();
    }

    public static void registerFrameMenu(ICommunication remoteObject) throws RemoteException {
        System.out.println("----------------REGISTER FRAME MENU---------------");
        System.out.println("Please choose data frame type");
        System.out.println("[1] Spectrum");
        System.out.println("[2] TimeHistory");
        System.out.println("[3] <-Back");
        System.out.println("--------------------------------------------------");

        switch (enterNumber()) {
            case 1:
                registerNewSpectrum(remoteObject);
                break;
            case 2:
                registerNewTimeHistory(remoteObject);
                break;
            case 3:
                break;
            default:
                registerFrameMenu(remoteObject);
                break;
        }
    }

    public static void registerNewSpectrum(ICommunication remoteObject) throws RemoteException {
        System.out.println("------------------REGISTER SPECTRUM---------------");
        System.out.println("Please choose data frame type");
        System.out.println("[1] Default");
        System.out.println("[2] Custom");
        System.out.println("[3] <- Back");
        System.out.println("--------------------------------------------------");

        switch (enterNumber()) {
            case 1:
                remoteObject.registerData(new Spectrum<Integer>());
                System.out.println("[+] Successful registration default Spectrum frame.\n");
                break;

            case 2:
                System.out.println("<= Enter device: ");
                String device = enterString();
                System.out.println("<= Enter description: ");
                String description = enterString();
                System.out.println("<= Enter date: ");
                long date = enterNumber();
                System.out.println("<= Enter channelNr: ");
                int channelNr = enterNumber();
                System.out.println("<= Enter unit: ");
                String unit = enterString();
                System.out.println("<= Enter resolution: ");
                double resolution = enterNumber();
                Integer[] buffer = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
                System.out.println("<= Enter scalling: ");
                int scaling = enterNumber();

                Spectrum<Integer> spectrum = new Spectrum(device, description, date, channelNr, unit, resolution, buffer, scaling);
                remoteObject.registerData(spectrum);
                System.out.println("[+] Successful registration custom Spectrum frame. \n");
                break;

            case 3:
                break;

            default:
                registerNewSpectrum(remoteObject);
                break;
        }
    }

    public static void registerNewTimeHistory(ICommunication remoteObject) throws RemoteException {
        System.out.println("---------------REGISTER TIME_HISTORY--------------");
        System.out.println("Please choose data frame type");
        System.out.println("[1] Default");
        System.out.println("[2] Custom");
        System.out.println("[3] <- Back");
        System.out.println("--------------------------------------------------");

        switch (enterNumber()) {
            case 1:
                remoteObject.registerData(new TimeHistory<Integer>());
                System.out.println("[+] Successful registration default TimeHistory frame.\n");
                break;

            case 2:
                System.out.println("<= Enter device: ");
                String device = enterString();
                System.out.println("<= Enter description: ");
                String description = enterString();
                System.out.println("<= Enter date: ");
                long date = enterNumber();
                System.out.println("<= Enter channelNr: ");
                int channelNr = enterNumber();
                System.out.println("<= Enter unit: ");
                String unit = enterString();
                System.out.println("<= Enter resolution: ");
                double resolution = enterNumber();
                Integer[] buffer = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
                System.out.println("<= Enter sensitivity: ");
                int sensitivity = enterNumber();

                TimeHistory<Integer> timeHistory = new TimeHistory(device, description, date, channelNr, unit, resolution, buffer, sensitivity);
                remoteObject.registerData(timeHistory);
                System.out.println("[+] Successful registration custom TimeHistory frame. \n");
                break;

            case 3:
                break;

            default:
                registerNewTimeHistory(remoteObject);
                break;
        }
    }

    public static void getData(ICommunication remoteObject) throws RemoteException {
        System.out.println("------------------GET DATA MENU------------------");
        System.out.println("Please choose data frame type");
        System.out.println("[1] Spectrum");
        System.out.println("[2] TimeHistory");
        System.out.println("[3] <-Back");
        System.out.println("--------------------------------------------------");

        switch (enterNumber()) {
            case 1:
                getSpectrumDataList(remoteObject);
                break;
            case 2:
                getTimeHistoryDataList(remoteObject);
                break;
            case 3:
                break;
            default:
                getData(remoteObject);
                break;
        }
    }

    public static void getSpectrumDataList(ICommunication remoteObject) throws RemoteException {
        String device;
        String description;
        long date = 0;
        List<Spectrum<Integer>> spectrumList = new Vector<Spectrum<Integer>>();

        System.out.println("------------GET DATA LIST MENU----------------");
        System.out.println("Please type data filter values: ");
        System.out.println("<== device filter: ");
        System.out.println("[if you don't want to filter this value please type NO]");
        device = enterString();
        System.out.println("<== description filter: ");
        System.out.println("[if you don't want to filter this value please type NO]");
        description = enterString();
        System.out.println("<== date filter: ");
        System.out.println("[if you don't want to filter this value please type NO]");
        String dateString = enterString();

        //filter date value
        if (!dateString.equals("NO")) {
            try {
                date = Integer.parseInt(dateString);
            } catch (NumberFormatException e) {
                System.out.println("[if you don't want to filter this value please type NO]");
                System.out.println("<== date filter: ");
                date = enterNumber();
            }
        }

        if (!device.equals("NO") && !description.equals("NO") && !dateString.equals("NO")) {
            spectrumList = remoteObject.getSpectrumDataList(device, description, date);
            printSpectrumList(spectrumList);
            //TODO menu saveToFile
            //TODO get specific number of data frame list
        } else if (!device.equals("NO") && !description.equals("NO")) {
            spectrumList = remoteObject.getSpectrumDataList(device);
            printSpectrumList(spectrumList);
        } else if (!device.equals("NO") && dateString.equals("NO")) {
            spectrumList = remoteObject.getSpectrumDataList(device);
            printSpectrumList(spectrumList);
        } else if (device.equals("NO") && description.equals("NO") && dateString.equals("NO")) {
            spectrumList = remoteObject.getSpectrumDataList();
            printSpectrumList(spectrumList);
        }

        dataOperationsMenu(remoteObject, spectrumList.size(), 0);
    }

    public static void getTimeHistoryDataList(ICommunication remoteObject) throws RemoteException {
        String device;
        String description;
        long date = 0;
        List<TimeHistory<Integer>> timeHistoryList = new Vector<TimeHistory<Integer>>();

        System.out.println("------------GET DATA LIST MENU----------------");
        System.out.println("Please type data filter values: ");
        System.out.println("<== device filter: ");
        System.out.println("[if you don't want to filter this value please type NO]");
        device = enterString();
        System.out.println("<== description filter: ");
        System.out.println("[if you don't want to filter this value please type NO]");
        description = enterString();
        System.out.println("<== date filter: ");
        System.out.println("[if you don't want to filter this value please type NO]");
        String dateString = enterString();

        //filter date value
        if (!dateString.equals("NO")) {
            try {
                date = Integer.parseInt(dateString);
            } catch (NumberFormatException e) {
                System.out.println("[if you don't want to filter this value please type NO]");
                System.out.println("<== date filter: ");
                date = enterNumber();
            }
        }

        if (!device.equals("NO") && !description.equals("NO") && !dateString.equals("NO")) {
            timeHistoryList = remoteObject.getTimeHistoryDataList(device, description, date);
            printTimeHistoryList(timeHistoryList);
        } else if (!device.equals("NO") && !description.equals("NO")) {
            timeHistoryList = remoteObject.getTimeHistoryDataList(device);
            printTimeHistoryList(timeHistoryList);
        } else if (!device.equals("NO") && dateString.equals("NO")) {
            timeHistoryList = remoteObject.getTimeHistoryDataList(device);
            printTimeHistoryList(timeHistoryList);
        } else if (device.equals("NO") && description.equals("NO") && dateString.equals("NO")) {
            timeHistoryList = remoteObject.getTimeHistoryDataList();
            printTimeHistoryList(timeHistoryList);
        }

        dataOperationsMenu(remoteObject, timeHistoryList.size(), 1);
    }

    public static void dataOperationsMenu(ICommunication remoteObject, int listSize, int type) throws RemoteException {

        System.out.println("\n---------DATA OPERATIONS---------");
        System.out.println("1. Save data by index");
        System.out.println("2. Download data by index");
        switch (enterNumber()) {
            case 1:
                if (listSize > 0) {
                    System.out.println("Please enter index You want to save in Server: ");
                    int index = enterNumber();

                    while (index > listSize) {
                        System.out.println("Incorrect index. Type again: ");
                        index = enterNumber();
                    }
                    if (type == 0)
                        remoteObject.saveSpectrumData(index);
                    else
                        remoteObject.saveTimeHistoryData(index);
                    System.out.println("[+] Data saved correctly.");
                } else System.out.println("[!] Data list is empty! Cannot save data!");
                break;
            case 2:
                if (listSize > 0) {
                    System.out.println("Please enter index You want to download from Server: ");
                    int downloadIndex = enterNumber();

                    while (downloadIndex > listSize) {
                        System.out.println("Incorrect index. Type again: ");
                        downloadIndex = enterNumber();
                    }
                    System.out.println("==> Download data");
                    if (type == 0)
                        System.out.println(remoteObject.getSpectrumData(downloadIndex));
                    else
                        System.out.println(remoteObject.getTimeHistoryData(downloadIndex));
                    System.out.println("[+] Data downloaded correctly.");
                } else System.out.println("[!] Data list is empty! Cannot download data!");
                break;
            default:
                break;
        }
    }

    public static int enterNumber() {
        Scanner userInput = new Scanner(System.in);
        String input = "";
        int result = 0;
        while (true) {
            System.out.print("User input: ");
            if (userInput.hasNextLine())
                input = userInput.nextLine();
            try {
                result = Integer.parseInt(input);
                break;
            } catch (NumberFormatException e) {
                System.out.println("[!] Incorrect input type");
            }
        }
        return result;
    }

    public static String enterString() {
        Scanner userInput = new Scanner(System.in);
        String input = "";
        if (userInput.hasNextLine())
            input = userInput.nextLine();

        return input;
    }

    public static void printSpectrumList(List<Spectrum<Integer>> spectrumList) {
        System.out.println("-------------------");
        System.out.println("Received Spectrum Data: " + spectrumList.size());
        for (int i = 0; i < spectrumList.size(); i++) {
            System.out.println();
            System.out.println("Element index: " + i);
            System.out.println(spectrumList.get(i));
            System.out.println();
        }
        for (int i = 0; i < 19; i++) {
            System.out.print("-");
            try {
                TimeUnit.MILLISECONDS.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println();
    }

    public static void printTimeHistoryList(List<TimeHistory<Integer>> timeHistoryList) {
        System.out.println("-------------------");
        System.out.println("Received TimeHistory Data: ");
        for (int i = 0; i < timeHistoryList.size(); i++) {
            System.out.println();
            System.out.println("Element index: " + i);
            System.out.println(timeHistoryList.get(i));
            System.out.println();
        }
        for (int i = 0; i < 19; i++) {
            System.out.print("-");
            try {
                TimeUnit.MILLISECONDS.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println();
    }
}

