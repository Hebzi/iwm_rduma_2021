package Lab04_RMI.Server;

import Lab02.*;

import Lab04_RMI.Interfejsy.ICommunication;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.*;

public class Servant extends UnicastRemoteObject implements ICommunication {
    public List<Spectrum<Integer>> spectrumContainer = new Vector<Spectrum<Integer>>();
    public List<TimeHistory<Integer>> timeHistoryContainer = new Vector<TimeHistory<Integer>>();
    public ExecutorService pool = Executors.newFixedThreadPool(20);

    public Servant() throws RemoteException {

    }

    @Override
    public void registerData(Object object) {
        new Thread(() -> {
            if (Tools.getClassType(object).equals("Spectrum")) {
                Tools.addSpectrumData((Spectrum<Integer>) object, spectrumContainer);
            } else if (Tools.getClassType(object).equals("TimeHistory")) {
                Tools.addTimeHistoryData((TimeHistory<Integer>) object, timeHistoryContainer);
            } else {
                System.out.println("[SERVER] Registration error!");
            }
        }).start();
    }

    //<-------------------------------------FILTER LISTS-------------------------------------
    @Override
    public List<Spectrum<Integer>> getSpectrumDataList() throws RemoteException {
        Future<List<Spectrum<Integer>>> spectrumList = pool.submit(new Callable<List<Spectrum<Integer>>>() {
            @Override
            public List<Spectrum<Integer>> call() {
                return spectrumContainer;
            }
        });
        try {
            return spectrumList.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Spectrum<Integer>> getSpectrumDataList(String deviceFilter) throws RemoteException {
        Future<List<Spectrum<Integer>>> spectrumList = pool.submit(new Callable<List<Spectrum<Integer>>>() {
            @Override
            public List<Spectrum<Integer>> call() {
                return Tools.getFilteredSpectrum(spectrumContainer, deviceFilter);
            }
        });
        try {
            return spectrumList.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Spectrum<Integer>> getSpectrumDataList(String deviceFilter, String descriptionFilter) throws RemoteException {
        Future<List<Spectrum<Integer>>> spectrumList = pool.submit(new Callable<List<Spectrum<Integer>>>() {
            @Override
            public List<Spectrum<Integer>> call() {
                return Tools.getFilteredSpectrum(spectrumContainer, deviceFilter, descriptionFilter);
            }
        });
        try {
            return spectrumList.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Spectrum<Integer>> getSpectrumDataList(String deviceFilter, String descriptionFilter, long dateFilter) throws RemoteException {
        Future<List<Spectrum<Integer>>> spectrumList = pool.submit(new Callable<List<Spectrum<Integer>>>() {
            @Override
            public List<Spectrum<Integer>> call() {
                return Tools.getFilteredSpectrum(spectrumContainer, deviceFilter, descriptionFilter, dateFilter);
            }
        });
        try {
            return spectrumList.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<TimeHistory<Integer>> getTimeHistoryDataList() throws RemoteException {
        Future<List<TimeHistory<Integer>>> timeHistoryFuture = pool.submit(new Callable<List<TimeHistory<Integer>>>() {
            @Override
            public List<TimeHistory<Integer>> call() {
                return timeHistoryContainer;
            }
        });
        try {
            return timeHistoryFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<TimeHistory<Integer>> getTimeHistoryDataList(String deviceFilter) throws RemoteException {
        Future<List<TimeHistory<Integer>>> timeHistoryFuture = pool.submit(new Callable<List<TimeHistory<Integer>>>() {
            @Override
            public List<TimeHistory<Integer>> call() {
                return Tools.getFilteredTimeHistory(timeHistoryContainer, deviceFilter);
            }
        });
        try {
            return timeHistoryFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<TimeHistory<Integer>> getTimeHistoryDataList(String deviceFilter, String descriptionFilter) throws RemoteException {
        Future<List<TimeHistory<Integer>>> timeHistoryFuture = pool.submit(new Callable<List<TimeHistory<Integer>>>() {
            @Override
            public List<TimeHistory<Integer>> call() {
                return Tools.getFilteredTimeHistory(timeHistoryContainer, deviceFilter, descriptionFilter);
            }
        });
        try {
            return timeHistoryFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<TimeHistory<Integer>> getTimeHistoryDataList(String deviceFilter, String descriptionFilter, long dateFilter) throws RemoteException {
        Future<List<TimeHistory<Integer>>> timeHistoryFuture = pool.submit(new Callable<List<TimeHistory<Integer>>>() {
            @Override
            public List<TimeHistory<Integer>> call() {
                return Tools.getFilteredTimeHistory(timeHistoryContainer, deviceFilter, descriptionFilter, dateFilter);
            }
        });
        try {
            return timeHistoryFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }
    //<------------------------------------------------------------------------------------

    @Override
    public Spectrum getSpectrumData(int itemPosition) throws RemoteException {
        Future<Spectrum<Integer>> spectrumFuture = pool.submit(new Callable<Spectrum<Integer>>() {
            @Override
            public Spectrum<Integer> call() {
                return spectrumContainer.get(itemPosition);
            }
        });
        try {
            return spectrumFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public TimeHistory<Integer> getTimeHistoryData(int itemPosition) throws RemoteException {
        Future<TimeHistory<Integer>> timeHistoryFuture = pool.submit(new Callable<TimeHistory<Integer>>() {
            @Override
            public TimeHistory<Integer> call() {
                return timeHistoryContainer.get(itemPosition);
            }
        });
        try {
            return timeHistoryFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean saveSpectrumData(int itemPosition) throws RemoteException {
        Future<Boolean> booleanFuture = pool.submit(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return Tools.savePacketToFile(spectrumContainer.get(itemPosition));
            }
        });
        try {
            return booleanFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean saveTimeHistoryData(int itemPosition) throws RemoteException {
        Future<Boolean> booleanFuture = pool.submit(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return Tools.savePacketToFile(timeHistoryContainer.get(itemPosition));
            }
        });
        try {
            return booleanFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return false;
        }
    }
}
