package Lab04_RMI.Interfejsy;

import Lab02.*;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;


public interface ICommunication extends Remote {
    //register data frame TimeHistory/Spectrum type
    public void registerData(Object object) throws RemoteException;

    //return data lists which contains correct values
    public List<Spectrum<Integer>> getSpectrumDataList() throws RemoteException;

    public List<Spectrum<Integer>> getSpectrumDataList(String device) throws RemoteException;

    public List<Spectrum<Integer>> getSpectrumDataList(String device, String description) throws RemoteException;

    public List<Spectrum<Integer>> getSpectrumDataList(String device, String description, long date) throws RemoteException;

    public List<TimeHistory<Integer>> getTimeHistoryDataList() throws RemoteException;

    public List<TimeHistory<Integer>> getTimeHistoryDataList(String device) throws RemoteException;

    public List<TimeHistory<Integer>> getTimeHistoryDataList(String device, String description) throws RemoteException;

    public List<TimeHistory<Integer>> getTimeHistoryDataList(String device, String description, long date) throws RemoteException;

    //return data object which contains correct values
    public Spectrum<Integer> getSpectrumData(int itemPosition) throws RemoteException;

    public TimeHistory<Integer> getTimeHistoryData(int itemPosition) throws RemoteException;

    //save data object on server which contains correct values
    public boolean saveSpectrumData(int itemPosition) throws RemoteException;

    public boolean saveTimeHistoryData(int itemPosition) throws RemoteException;
}
