package Lab03;

import Lab02.*;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.concurrent.TimeUnit;

public class UDPClient {
    public UDPClient() {
    }

    public static void main(String[] args) throws InterruptedException {

        DatagramSocket socket = null;
        Integer[] pom = {1, 3, 5, 4, -3, 5, -5, 1, 0, -3, 4, 3, 1, -4, 2, -2};
        Spectrum packet = new Spectrum("Nazwa", "Opis", 12312312L, 3, "unit", 123.23D, new Double[]{1.0D, 2.0D, 3.0D, 4.0D}, 0);
        TimeHistory<Integer> t2 = new TimeHistory<Integer>("Urządzenie", "Opis urządzenia", 1000, 15, "unit", 0.5, pom, 0.4);
        Request reqSpectrum = new Request("Nazwa", "Opis", 122131213, 0, 1);
        Request reqTH = new Request("Nazwa2", "Opis2", 122, 0, 1000);

        try {
            InetAddress aHost = InetAddress.getLocalHost();
            int serverPort = 9876;
            socket = new DatagramSocket();

            while (true) {
                sendData(packet, socket, aHost, serverPort, 500);
                sendData(t2, socket, aHost, serverPort, 500);
                sendRequest(reqTH, socket, aHost, serverPort, 500);
                sendRequest(reqSpectrum, socket, aHost, serverPort, 500);
            }
        } catch (IOException var19) {
            var19.printStackTrace();
        } finally {
            socket.close();
        }

    }

    public static Packet sendRequest(Request requestPacket, DatagramSocket socket, InetAddress aHost, int port, int timestamp) {
        try {
            socket.setSoTimeout(2000);
            aHost = InetAddress.getLocalHost();
            byte[] data = Tools.serialize(requestPacket);
            DatagramPacket request = new DatagramPacket(data, data.length, aHost, port);
            socket.send(request);
            System.out.println("Wysłano prosbe \n");
            byte[] buffer = new byte[1024];
            DatagramPacket response = new DatagramPacket(buffer, buffer.length);
            socket.receive(response);
            System.out.println("Otrzymano: \n" + Tools.deserialize(buffer) + "\n");
            TimeUnit.MILLISECONDS.sleep(timestamp);
            if (Tools.deserialize(buffer).equals("[SERVER] Bład!")) {
                return null;
            } else {
                return (Packet) Tools.deserialize(buffer);
            }

        } catch (IOException | InterruptedException | ClassNotFoundException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static void sendData(Packet packet, DatagramSocket socket, InetAddress aHost, int port, int timestamp) {
        Request requestPacket = new Request();
        try {
            aHost = InetAddress.getLocalHost();
            byte[] data = Tools.serialize(requestPacket);
            DatagramPacket request = new DatagramPacket(data, data.length, aHost, port);
            socket.send(request);
            TimeUnit.MILLISECONDS.sleep(20);
            data = Tools.serialize(packet);
            request = new DatagramPacket(data, data.length, aHost, port);
            socket.send(request);
            System.out.println("Wysłano: " + Tools.deserialize(request.getData()).toString() + "\n");
            TimeUnit.MILLISECONDS.sleep(timestamp);
        } catch (IOException | InterruptedException | ClassNotFoundException var1) {
            System.out.println("[Wyslano] " + var1.toString() + "\n");
        }
    }

}