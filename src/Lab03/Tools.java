package Lab03;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Tools extends Thread {
    public Tools() {
    }

    public static byte[] serialize(Object obj) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream out = null;

        Object var4;
        try {
            out = new ObjectOutputStream(bos);
            out.writeObject(obj);
            out.flush();
            byte[] var3 = bos.toByteArray();
            return var3;
        } catch (IOException var14) {
            var14.printStackTrace();
            var4 = null;
        } finally {
            try {
                bos.close();
            } catch (IOException var13) {
                var13.printStackTrace();
            }

        }

        return (byte[]) var4;
    }

    public static Object deserialize(byte[] bytes) throws ClassNotFoundException {
        Object obj = null;

        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
            ObjectInputStream in = new ObjectInputStream(bis);
            obj = in.readObject();
            in.close();
            return obj;
        } catch (IOException var4) {
            var4.printStackTrace();
            return obj;
        }
    }
}
