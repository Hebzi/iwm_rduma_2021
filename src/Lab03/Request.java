package Lab03;

import Lab02.Packet;

public class Request extends Packet {
    public final int type; // 0 - save, 1 - read
    public final int value;

    public Request(String device, String description, long date, int type, int value) {
        super(device, description, date);
        this.type = type;
        this.value = value;
    }

    public Request() {
        super();
        this.type = 0;
        this.value = 0;
    }

    @Override
    public String toString() {
        return "Request{" +
                "device='" + device + '\'' +
                ", description='" + description + '\'' +
                ", date=" + date +
                ", type=" + type +
                ", value=" + value +
                '}';
    }
}
