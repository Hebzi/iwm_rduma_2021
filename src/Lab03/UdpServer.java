package Lab03;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

import Lab02.Packet;

public class UdpServer {
    public UdpServer() {
    }

    public static void main(String[] args) {
        DatagramSocket Socket = null;

        try {
            InetAddress aHost = InetAddress.getLocalHost();
            Socket = new DatagramSocket(9876, aHost);
            byte[] buffer = new byte[1024];

            while (true) {
                DatagramPacket request = new DatagramPacket(buffer, buffer.length);
                System.out.println("Waiting for request...");
                Socket.receive(request);

                try {

                    Request req = (Request) Tools.deserialize(buffer);
                    if (req.type == 0) {
                        System.out.println("Odebrano dane \n");
                        Socket.receive(request);
                        Packet save = (Packet) Tools.deserialize(buffer);
                        String file_format = ".txt";
                        String var10000 = save.getDevice();
                        String file_name = var10000 + "_" + save.getDescription() + "_" + save.getDate() + file_format;
                        OutputStream os = new FileOutputStream(file_name);
                        os.write(Tools.serialize(save));
                        os.close();
                        System.out.println("Received: \n" + save + "\n\n");
                    } else if (req.type == 1) {
                        System.out.println(" Prosba od klienta o dane: " + "Nazwa urządzenia: " + req.getDevice() + ", Opis: " + req.getDescription() + ", Dane: " + req.getDate());
                        String file_format = ".txt";
                        String file_name = req.getDevice() + "_" + req.getDescription() + "_" + req.getDate() + file_format;
                        InputStream is = new FileInputStream(file_name);
                        byte[] content = is.readAllBytes();
                        is.close();
                        System.out.println("Odnaleziono prośbę: \n" + Tools.deserialize(content).toString());
                        //send msg
                        DatagramPacket response = new DatagramPacket(content, content.length, request.getAddress(), request.getPort());
                        Socket.send(response);
                        System.out.println("Wysłanie danych do klienta: \n" + Tools.deserialize(content));
                    } else {
                        System.out.println("Błąd \n");
                    }
                } catch (ClassNotFoundException var14) {
                    Logger.getLogger(UDPClient.class.getName()).log(Level.SEVERE, null, var14);
                }

                DatagramPacket reply = new DatagramPacket(request.getData(), request.getLength(), request.getAddress(), request.getPort());
                Socket.send(reply);
            }
        } catch (IOException var15) {
            Logger.getLogger(UdpServer.class.getName()).log(Level.SEVERE, null, var15);
        } finally {
            assert Socket != null;
            Socket.close();
        }

    }
}